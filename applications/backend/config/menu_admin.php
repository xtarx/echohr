<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Menu
| -------------------------------------------------------------------------
| This file lets you define navigation menu items on sidebar.
|
*/

$config['menu'] = array(

	'home' => array(
		'name'      => 'Home',
		'url'       => site_url(),
		'icon'      => 'fa fa-home'
	),

	'user' => array(
		'name'      => 'Users',
		'url'       => site_url('user'),
		'icon'      => 'fa fa-users'
	),
	'Employees' => array(
		'name'      => 'Employees',
		'url'       => site_url('employees'),
		'icon'      => 'fa fa-cogs'
	),
	'Salaries' => array(
		'name'      => 'Salaries',
		'url'       => site_url('salary'),
		'icon'      => 'fa fa-usd'
	),
	'Deductions' => array(
		'name'      => 'Deductions',
		'url'       => site_url('salary_deductions'),
		'icon'      => 'fa fa-eraser'
	),
	'Deduction types' => array(
		'name'      => 'Deduction types',
		'url'       => site_url('deduction_types'),
		'icon'      => 'fa fa-files-o'
	),

	'Departments' => array(
		'name'      => 'Departments',
		'url'       => site_url('departments'),
		'icon'      => 'fa fa-building-o'
	),

	// // Example to add sections with subpages
	// 'example' => array(
	// 	'name'      => 'Employees',
	// 	'url'       => site_url('example'),
	// 	'icon'      => 'fa fa-cog',
	// 	'children'  => array(
	// 		'Salaries'		=> site_url('salary'),
	// 		'Deductions'		=> site_url('salary_deductions'),
	// 		'Departments'		=> site_url('departments'),
	// 	)
	// ),
	// end of example

	// 'admin' => array(
	// 	'name'      => 'Administration',
	// 	'url'       => site_url('admin'),
	// 	'icon'      => 'fa fa-cog',
	// 	'children'  => array(
	// 		'Departments'		=> site_url('departments'),
	// 	)
	// ),

	'logout' => array(
		'name'      => 'Sign out',
		'url'       => site_url('account/logout'),
		'icon'      => 'fa fa-sign-out'
	),
);