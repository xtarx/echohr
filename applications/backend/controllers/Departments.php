<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departments extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('department_model', 'departments');
	}

	public function index()
	{
		// CRUD table
		$this->load->helper('crud');
		$crud = generate_crud('departments');		
		$crud->set_rules('name','name','required');
		$crud->unset_delete();

		$this->mTitle = "Departments";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}


	function upload()
	{

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '100000';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			
			set_alert('danger', 'Upload Failed make sure format is csv or xls.');
			redirect('home');

			// $this->load->view('upload_form', $error);
		}
		else
		{
			$upload_data= $this->upload->data();
			$data = array('upload_data' =>$upload_data);
			
			$file_path=$upload_data['full_path'];

			$this->import($file_path);			
			// redirect('departments');

			// print_r($data);
			// $this->load->view('upload_success', $data);
		}
	}


	public function import($file_path)
	{
		$this->load->library('csvreader');
		$result =   $this->csvreader->parse_file($file_path);
		$count=0;
		foreach($result as $field){

			if(!empty($field['name'])){
				$found=$this->departments->get_by( array('name' => $field['name'], ));			
				if(!$found){
					$count++;
					$this->departments->insert( array('name' => $field['name'], ));			
				}
			}

		}
		// print_r($result);

		//toast success
		// set_alert('danger', 'Invalid Login.');
		if($count>0){
			set_alert('success', $count.' records have been imported succsefully');
		}else{
			set_alert('success','records have been imported succsefully');

		}

		redirect('departments');


	}
	


}