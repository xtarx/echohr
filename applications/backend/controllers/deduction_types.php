<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deduction_types extends MY_Controller {

	public function index()
	{
		// CRUD table
		$this->load->helper('crud');
		$crud = generate_crud('deduction_types');
		$crud->unset_fields( 'created_at');
		$crud->unset_columns( 'created_at');
		// $crud->set_relation('user_id','users','name');

		$crud->set_rules('name','name','required');

		$this->mTitle = "Deduction Types";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}

	function upload()
	{

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '100000';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			set_alert('danger', 'Upload Failed make sure format is csv or xls.');
			redirect('home');

			// $this->load->view('upload_form', $error);
		}
		else
		{
			$upload_data= $this->upload->data();
			$data = array('upload_data' =>$upload_data);
			
			$file_path=$upload_data['full_path'];
			// echo $file_path;
			$this->import($file_path);			
			// redirect('departments');

			// print_r($data);
			// $this->load->view('upload_success', $data);
		}
	}


	public function import($file_path)
	{
		// $file_path='/Volumes/Data/Work/htdocs/NgageU/echoHR/uploads/departments6.csv';
		$this->load->library('csvreader');
		$this->load->model('deductiontypes_model', 'deductions');

		$result =   $this->csvreader->parse_file($file_path);
		$count=0;

		foreach($result as $field){

			if(!empty($field['name'])){
				$found=$this->deductions->get_by( array('name' => $field['name'], ));			
				if(!$found){
					$count++;
					$this->deductions->insert( array('name' => $field['name'], ));			
				}
			}

		}
		// print_r($result);

		if($count>0){
			set_alert('success', $count.' records have been imported succsefully');
		}else{
			set_alert('success','records have been imported succsefully');

		}
		redirect('deduction_types');


	}
	
	
}