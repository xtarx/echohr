<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employees extends MY_Controller {

	public function index()
	{
		// $this->output->enable_profiler(TRUE);

		// CRUD table
		$this->load->helper('crud');
		$crud = generate_crud('employees');
		// $crud->unset_fields('from_user_id', 'deduction');
		$crud->unset_fields( 'allowances','benefits','salary','joinDate');
		$crud->unset_columns( 'joinDate','salary','allowances','benefits');
		
		// $crud->unset_add();
		// $crud->unset_edit();
		$crud->unset_delete();
		// $crud->unset_read();
		
		$crud->display_as('id','name');


		$crud->set_relation('id','users','name');
		$crud->set_relation('department','departments','name');

		$this->mTitle = "Employees";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}

	

	


}