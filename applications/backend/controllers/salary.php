<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary extends MY_Controller {

	public function index()
	{
		// $this->output->enable_profiler(TRUE);

		// CRUD table
		$this->load->helper('crud');
		$crud = generate_crud('salaries');
		// $crud->unset_fields('from_user_id', 'deduction');
		$crud->unset_fields( 'deduction','created_at');
		
		$user=get_user();
		
		$crud->field_type('from_user_id', 'hidden', $user['id']);

		$crud->columns('user_id', 'salary','date' );

		$crud->display_as('user_id','Employee');
		$crud->display_as('date','Effective Date');
		$crud->display_as('file_url','File');

		$crud->set_relation('user_id','users','name');

		$crud->set_field_upload('file_url','uploads/files');

		$crud->set_rules('salary','salary','required|numeric');
		$crud->set_rules('user_id','Employee','required');
		$crud->set_rules('date','date','required');


		$this->mTitle = "Salaries";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}

	

	


}