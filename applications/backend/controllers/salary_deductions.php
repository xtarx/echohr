<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary_deductions extends MY_Controller {

	public function index()
	{
		// CRUD table
		$this->load->helper('crud');
		$crud = generate_crud('salary_deductions');
		$crud->unset_fields( 'deduction','created_at');
		$crud->columns('user_id', 'amount', 'date');
		$crud->set_relation('user_id','users','name');
		$user=get_user();

		$crud->field_type('by_user_id', 'hidden', $user['id']);

		$crud->display_as('user_id','Employee');
		$crud->display_as('date','Effective Date');
		$crud->display_as('file_url','File');

		$crud->set_relation('type','deduction_types','name');


		$crud->set_rules('amount','amount','required|numeric');
		$crud->set_rules('user_id','Employee','required');
		$crud->set_rules('date','date','required');


		$crud->set_relation('user_id','users','name');

		$crud->set_field_upload('file_url','uploads/files');

		$this->mTitle = "Salary deductions";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}
	
}