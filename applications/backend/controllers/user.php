<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// .invite employers (add entry in employers)
// once pressed the actiation link
// .set password and fill employee data
// 
// .view salary 
// //.

class User extends MY_Controller {

	public function index()
	{
		// CRUD table
		$this->load->helper('crud');

		$crud = generate_crud('users');

		$crud->unset_fields('password', 'forgot_password_code', 'forgot_password_time', 'active','last_signin','role');
		$crud->field_type('activation_code', 'hidden', generate_unique_code());
		$crud->field_type('created_at', 'hidden', date('Y-m-d H:i:s'));
		$crud->set_rules('email','email','required|valid_email|is_unique[users.email]');
		$crud->set_rules('name','name','required');
		$crud->edit_fields('name');

		$crud->unset_delete();

		// $crud->set_relation('department','departments','name');

		$crud->columns('role', 'name', 'email', 'active','activation_code', 'created_at');
		
		$this->mTitle = "Users";
		$this->mViewFile = '_partial/crud';
		$this->mViewData['crud_data'] = $crud->render();
	}
	
	/**
	 * Grocery Crud callback functions
	 */
	public function callback_before_create_user($post_array)
	{

		// if($post_array['role']=="regular"){
		// 	$post_array['activation_code'] = generate_unique_code();
		// }else{
		// 	//admin case 
		// 	$post_array['password'] = hash_pw($post_array['password']);
		// }
		$post_array['password'] = "testing";

		// $post_array['activation_code'] = generate_unique_code();
		// $post_array['created_at'] = date('Y-m-d H:i:s');

		return $post_array;
	}
	
	/**
	 * Grocery Crud callback functions
	 */
	public function callback_after_create_user($post_array,$primary_key)
	{


		$this->load->helper('email');

		//invite user, then create an employee entry
		$this->load->model('employee_model', 'employees');
		$user = $this->employees->insert( array(
			'id' =>$primary_key , 			
			));

		
				// send activation email (make sure config/email.php is properly set first)
		
		$to_name = $post_array['name'];
		$subject = 'Account Activation';
		send_email($post_array['email'], $to_name, $subject, 'signup', $post_array);
		

		return true;


	}


	public function con()
	{
		$this->load->helper('email');

		echo "hi mate";
		$to_name = "asdas";
		$subject = 'Account Activation';
		$post_array['activation_code']="asdasd";
		echo	send_email("mtarek@ngageu.com", $to_name, $subject, 'signup', $post_array);		
	}

	function upload()
	{

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '100000';
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			set_alert('danger', 'Upload Failed make sure format is csv or xls.');
			redirect('home');
			// $this->load->view('upload_form', $error);
		}
		else
		{
			$upload_data= $this->upload->data();
			$data = array('upload_data' =>$upload_data);
			
			$file_path=$upload_data['full_path'];

			$this->import($file_path);			
			// redirect('departments');

			// print_r($data);
			// $this->load->view('upload_success', $data);
		}
	}


	public function import($file_path)
	{
		$this->load->library('csvreader');
		$this->load->model('user_model', 'users');
		$this->load->model('department_model', 'departments');
		$this->load->model('employee_model', 'employees');

		$result =   $this->csvreader->parse_file($file_path);

		// print_r($result);

		$count=0;

		foreach($result as $field){

			if(!empty($field['email'])){
				$found=$this->users->get_by( array('email' => $field['email'], ));			
				if(!$found){

					//department matching name to id
					$department_id=1;
					if($field['department']){
					 	//if found get its ID, else create 
						$found=$this->departments->get_by( array('name' => $field['department'], ));			
						if($found){	
							//create
							$department_id=$this->departments->insert( array('name' => $field['department'], ));
						}else{
							$department_id=$found['id'];
						}

					}

					$imported_user=$this->users->insert( array(
						'email' => $field['email'],
						'name' => $field['name'],
						'department' => $department_id,
						'activation_code' =>  generate_unique_code(),
						'created_at' => date('Y-m-d H:i:s'),


						));		
					//add employee entry
					$user = $this->employees->insert( array('id' =>$imported_user ));
					$count++;
				}
			}

		}
		// print_r($result);

		if($count>0){
			set_alert('success', $count.' records have been imported succsefully');
		}else{
			set_alert('success','records have been imported succsefully');

		}
		
		redirect('user');


	}
	
}