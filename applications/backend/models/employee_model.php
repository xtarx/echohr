<?php

/**
 * https://github.com/jamierumbelow/codeigniter-base-model
 */
class Employee_model extends MY_Model
{
	public $_table = 'employees';
	public $return_type = 'array';
}