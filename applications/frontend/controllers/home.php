<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function index()
	{
		// $this->output->enable_profiler(TRUE);

		$this->mLayout = "home";
		$this->mTitle = "Home";
		$this->mViewFile = 'home';

		//if user logged in
		//fetch salary detials

		$logged_user=get_user();
		$this->mViewData['logged']=false;
		if($logged_user){
			$this->mViewData['logged']=true;

			
		//if admin redirect to backend
			
			if($logged_user ['role']=='admin'){
				redirect('backend.php');
			}

			
			$this->load->model('SalaryDeductions_model', 'salaryDeductions');
			$this->load->model('Salary_model', 'salary');

			$deductions=$this->salaryDeductions->get_many_by( array('user_id' => $logged_user['id'] ));
			$salary=$this->salary->get_by( array('user_id' => $logged_user['id'] ));

			$this->mViewData['deductions']=$deductions;
			if($salary)
				$this->mViewData['salarydetails']=$salary;


		}
		
		



	}
}