<?php

/**
 * https://github.com/jamierumbelow/codeigniter-base-model
 */
class Department_model extends MY_Model
{
	public $_table = 'departments';
	public $return_type = 'array';
}