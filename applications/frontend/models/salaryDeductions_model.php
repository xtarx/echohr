<?php

/**
 * https://github.com/jamierumbelow/codeigniter-base-model
 */
class SalaryDeductions_model extends MY_Model
{
	public $_table = 'salary_deductions';
	public $return_type = 'array';
}