<?php

/**
 * https://github.com/jamierumbelow/codeigniter-base-model
 */
class Salary_model extends MY_Model
{
	public $_table = 'salaries';
	public $return_type = 'array';
}