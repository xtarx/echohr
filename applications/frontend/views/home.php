
<!-- <div class="alert alert-success">
	This is the homepage.
</div>
-->
<?php if(!$logged): ?>

  <div class="alert alert-success">
    This is the homepage, Login to see Salary details.
  </div>




<?php elseif($logged &&isset($salarydetails )):?>


  <h2> Salary Deduction</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Date</th>
        <th>Amount</th>
        <th>File</th>

      </tr>
    </thead>
    <tbody>

     <?php 
     $total_deduction=0;
     foreach ($deductions as $key => $deduction) :?>
     <tr>
      <th scope="row"><?php echo $deduction['id'] ?></th>
      <td><?php echo $deduction['date'] ?></td>
      <td><?php echo $deduction['amount'];  $total_deduction+=$deduction['amount'];?></td>
      <?php if(!empty($deduction['file_url'])){ ?>

      <td><a href="<?php echo base_url('uploads/files/'. $deduction['file_url']); ?>">Download</a></td>

      <?php }else{ ?>
      <td>-</td>

      <?php }?>
    </tr>
  <?php endforeach; ?>

  <tr>
    <th scope="row"></th>
    <td>Total</td>
    <td><?php echo $total_deduction ?></td>
  </tr>
</tbody>
</table>
<?php if(!empty($salarydetails['file_url'])): ?>

   <a href="<?php echo base_url('uploads/files/'. $salarydetails['file_url']); ?>">Download Salary Information</a> 
<?php endif; ?>
<h2> Net Salary </h2> <h3> <?php echo $salarydetails['salary'] ?> - <?php echo $total_deduction ?> = <?php echo $salarydetails['salary']- $total_deduction ?></h3>

<?php  else:?>

  <div class="alert alert-success">
   You dont have any financial details yet.
 </div>

<?php  endif;?>

